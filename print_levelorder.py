#!/usr/bin/env python3

#           1
#         /   \
#        2     4
#      /   \  /  \
#     5    6  10  12
#    /            / \
#   8            9   3

from src.TreeBuilder import build_tree

print("Level order traversal")
print("Print one specific level")

print("Build the tree")
root = build_tree()


def calculate_height(node):
    if node:
        # each subtree height gets recursively calculated
        left_height = calculate_height(node.left)
        right_height = calculate_height(node.right)

        return max([left_height, right_height]) + 1
    else:
        return 0


def print_level_order(tree):
    h = calculate_height(tree)
    for level in range(1, h + 1):
        print_level(tree, level)


def print_level(node, level):
    if node is None:
        return

    if level == 1:
        print(node)
    else:
        print_level(node.left, level - 1)
        print_level(node.right, level - 1)

print("Print level 3")  # 5 6 10 12
print_level(root, 3)

print("Print level 4")  # 8 9 3
print_level(root, 4)

print("Print level 1")  # 1
print_level(root, 1)
