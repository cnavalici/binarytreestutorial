# Binary Tree Tutorial

These code snippets show different aspects involved with Binary Tree structures.

## Visiting all the nodes in a tree

There are three used patterns to visit all nodes:

- preorder traversal
- inorder traversal
- postorder traversal

Run __traverse_*.py_ files in order to get the results.

    python3 traverse_inorder.py

## Getting a specific level in a tree

Run

    python3 print_levelorder.py

    