#!/usr/bin/env python3

#           1
#         /   \
#        2     4
#      /   \  /  \
#     5    6  10  12
#    /            / \
#   8            9   3

from src.TreeBuilder import build_tree

print("Preorder traversal")
print("Visit the root node, then recursively do a preorder traversal on the left tree, ")
print("followed by a recursive preorder traversal on the right tree")

print("Build the tree")
root = build_tree()


def preorder(node):
    if node:
        print(node)
        preorder(node.left)
        preorder(node.right)


# 1 2 5 8 6 4 10 12 9 3
print("Run the PREORDER traversal")
preorder(root)