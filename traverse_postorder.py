#!/usr/bin/env python3

#           1
#         /   \
#        2     4
#      /   \  /  \
#     5    6  10  12
#    /            / \
#   8            9   3

from src.TreeBuilder import build_tree

print("Postorder traversal")
print("Recursive traversal of the left node, then recursive right node, then root.")

print("Build the tree")
root = build_tree()


def postorder(node):
    if node:
        postorder(node.left)
        postorder(node.right)
        print(node)


# 8 5 6 2 10 9 3 12 4 1
print("Run the POSTORDER traversal")
postorder(root)