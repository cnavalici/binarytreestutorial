#           1
#         /   \
#        2     4
#      /   \  /  \
#     5    6  10  12
#    /            / \
#   8            9   3

from .TreeNode import TreeNode


def build_tree():
    root = TreeNode(1)

    node2 = TreeNode(2)
    node4 = TreeNode(4)
    root.left = node2
    root.right = node4

    node5 = TreeNode(5)
    node6 = TreeNode(6)
    node2.left = node5
    node2.right = node6

    node5.left = TreeNode(8)

    node10 = TreeNode(10)
    node12 = TreeNode(12)
    node4.left = node10
    node4.right = node12

    node12.left = TreeNode(9)
    node12.right = TreeNode(3)

    return root
